package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.enfocat.javaweb.buscaminas.Buscaminas;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
      out.write('\n');
      out.write('\n');


    if (request.getParameter("reset")!=null){
        Buscaminas.reset(); 
    }

   if (request.getParameter("mostrar")!=null){
        Buscaminas.mostrar(); 
    }

    if (request.getParameter("x")!=null){
        
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.clicar(x,y); 
    }


      out.write("\n");
      out.write("  \n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
      out.write("    <title>Document</title>\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/estilos.css\">\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("<h1>Buscaminas JSP</h1>\n");
      out.write("\n");
      out.print( Buscaminas.htmlCampo() );
      out.write("\n");
      out.write("\n");
      out.write("<br>\n");
      out.write("<br>\n");
      out.write("<button id=\"reset\">Reset</button>\n");
      out.write("<button id=\"mostrar\">Mostrar</button>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" crossorigin=\"anonymous\"></script>\n");
      out.write(" \n");
      out.write("<script>\n");
      out.write("\n");
      out.write("$(document).on(\"click\", \"div.celda.oculta\", function(){\n");
      out.write("    let url = window.location.href.split(\"?\")[0];\n");
      out.write("    let x=$(this).data(\"x\");\n");
      out.write("    let y=$(this).data(\"y\");\n");
      out.write("    window.location.href = url + \"?x=\"+x+\"&y=\"+y;\n");
      out.write("})\n");
      out.write("\n");
      out.write("$(document).on(\"click\", \"button#reset\", function(){\n");
      out.write("    let url = window.location.href.split(\"?\")[0];\n");
      out.write("    window.location.href = url + \"?reset=1\";\n");
      out.write("})\n");
      out.write("\n");
      out.write("$(document).on(\"click\", \"button#mostrar\", function(){\n");
      out.write("    let url = window.location.href.split(\"?\")[0];\n");
      out.write("    window.location.href = url + \"?mostrar=1\";\n");
      out.write("})\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
