<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.javaweb.buscaminas.Buscaminas" %>

<%

    if (request.getParameter("reset")!=null){
        Buscaminas.reset(); 
    }

   if (request.getParameter("mostrar")!=null){
        Buscaminas.mostrar(); 
    }

    if (request.getParameter("x")!=null){
        
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.clicar(x,y); 
    }

%>
  

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>

<h1>Buscaminas JSP</h1>

<%= Buscaminas.htmlCampo() %>

<br>
<br>
<button id="reset">Reset</button>
<button id="mostrar">Mostrar</button>



<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
 
<script>

$(document).on("click", "div.celda.oculta", function(){
    let url = window.location.href.split("?")[0];
    let x=$(this).data("x");
    let y=$(this).data("y");
    window.location.href = url + "?x="+x+"&y="+y;
})

$(document).on("click", "button#reset", function(){
    let url = window.location.href.split("?")[0];
    window.location.href = url + "?reset=1";
})

$(document).on("click", "button#mostrar", function(){
    let url = window.location.href.split("?")[0];
    window.location.href = url + "?mostrar=1";
})
</script>

</body>
</html>