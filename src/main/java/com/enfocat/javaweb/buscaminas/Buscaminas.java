
package com.enfocat.javaweb.buscaminas;

import java.util.Random;

public class Buscaminas {

    private static final int ANCHO = 10;
    private static final int ALTO = 8;
    private static final int MINAS = 12;

    private static Celda[][] campo = generaCampo(MINAS);
    public int status = 0;

    // generar campo, al ejectuar por primera vez la aplicación
    private static Celda[][] generaCampo(int minas_a_crear) {

        Celda[][] celdas = new Celda[ALTO][ANCHO];

        // llenamos el campo de elementos "mina" desactivados
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Celda m = new Celda(false, x, y);
                celdas[y][x] = m;
            }
        }

        // asignamos minas aleatoriamente
        Random rnd = new Random();
        for (int m = 0; m < minas_a_crear; m++) {
            int rndX = rnd.nextInt(ANCHO);
            int rndY = rnd.nextInt(ALTO);
            celdas[rndY][rndX].setMina(true);
            // System.out.println("Mina: "+rndX+","+rndY);
        }

        // calculamos y asignamos posiciones "minas cercanas"
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                int minasCercanas = Buscaminas.buscaBombas(x, y, celdas);
                Celda m = celdas[y][x];
                m.setBombas(minasCercanas);
            }
        }
        return celdas;
    }

    public static Celda[][] getCampo() {
        return Buscaminas.campo;
    }


    //genera de nuevo el campo de minas
    public static void reset() {
        Buscaminas.campo = generaCampo(MINAS);
    }

    // aplica setvisible true a todas las minas
    public static void mostrar() {
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Buscaminas.campo[y][x].setVisible(true);
            }
        }
    }

    // CLICAMOS EN UNA MINA
    // Javascript impide que se clique en una mina ya visible
    public static void clicar(int x, int y) {
        Celda m = Buscaminas.campo[y][x];
        //se establece como visible
        m.setVisible(true);
        //si en esta mina NO hay bombas cercanas Y no es una mina, miramos alrededor
        if (m.getBombas()==0 && !m.isMina()) {
            alrededor(x, y);
        }
    }

    //verificamos alrededor de una posición x,y
    public static void alrededor(int x, int y){
        Buscaminas.revisa(x-1,y-1);
        Buscaminas.revisa(x,y-1);
        Buscaminas.revisa(x+1,y-1);
        Buscaminas.revisa(x-1,y);
        Buscaminas.revisa(x+1,y);
        Buscaminas.revisa(x-1,y+1);
        Buscaminas.revisa(x,y+1);
        Buscaminas.revisa(x+1,y+1);
    }

    /* revisamos una posición:
        1.si está fuera de rango, salimos
        2.si es una mina,  salimos
        3.si es una posición visible, salimos
        4.si no... clicamos en la posición
        */
    
    public static void revisa(int x, int y){

        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return; //res a fer
        }
        Celda pos = Buscaminas.campo[y][x];

        if (pos.isMina() || pos.isVisible()){
            return; //??
        }

        if (pos.getBombas()>0){
            pos.setVisible(true);
        }

        //si "0" clicamos posición
        if (pos.getBombas()==0){
            Buscaminas.clicar(x,y);
        }
       
    }


    public static boolean noEsBomba(int x, int y){
        return false;
    }

    private static int esMina(int x, int y, Celda[][] campo) {
        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return 0;
        }
        Celda m = campo[y][x];
        return m.isMina() ? 1 : 0;
    }


    public static int buscaBombas(int x, int y) {
        // cada posición puede tener como máximo 8 adyacentes
        Celda[][] celdas = Buscaminas.campo;
        return Buscaminas.esMina(x - 1, y - 1, celdas) + Buscaminas.esMina(x, y - 1, celdas)
                + Buscaminas.esMina(x + 1, y - 1, celdas) + Buscaminas.esMina(x - 1, y, celdas)
                + Buscaminas.esMina(x + 1, y, celdas) + Buscaminas.esMina(x - 1, y + 1, celdas)
                + Buscaminas.esMina(x, y + 1, celdas) + Buscaminas.esMina(x + 1, y + 1, celdas);
    }

    public static int buscaBombas(int x, int y, Celda[][] celdas) {
        // cada posición puede tener como máximo 8 adyacentes
        return Buscaminas.esMina(x - 1, y - 1, celdas) + Buscaminas.esMina(x, y - 1, celdas)
                + Buscaminas.esMina(x + 1, y - 1, celdas) + Buscaminas.esMina(x - 1, y, celdas)
                + Buscaminas.esMina(x + 1, y, celdas) + Buscaminas.esMina(x - 1, y + 1, celdas)
                + Buscaminas.esMina(x, y + 1, celdas) + Buscaminas.esMina(x + 1, y + 1, celdas);

    }

    public static String htmlCampo() {

        StringBuilder sb = new StringBuilder();

        String iniDivFila = "<div class='fila'>";
        String finDiv = "</div>";

        sb.append("<div class='campo'>");
        for (int y = 0; y < ALTO; y++) {
            sb.append(iniDivFila);
            for (int x = 0; x < ANCHO; x++) {
                Celda m = Buscaminas.campo[y][x];
                sb.append(m.html());
            }
            sb.append(finDiv);
        }
        sb.append(finDiv);

        return sb.toString();

    }

}