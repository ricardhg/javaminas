package com.enfocat.javaweb.buscaminas;

public class Celda {
    
    private static final boolean SHOWHINTS = true;

    private boolean visible;
    private int x;
    private int y;
    private boolean mina = false;
    public String texto;
    private int bombas;

    public Celda(boolean mina, int x, int y){
        this.mina = mina;
        this.x=x;
        this.y=y;
        this.visible=false;
        this.bombas = 0;
        this.texto = "-";
    }

    public String html(){
        String css = (this.mina) ? "celda mina " : "celda";
        String texto = (this.mina) ? "B" : String.valueOf(this.bombas);
        if (texto.equals("0")) texto = "";
        if(!this.visible) css+=" oculta";
        if (SHOWHINTS){
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%s</div>", css, this.x, this.y,texto);
        }else{
            return String.format("<div class='%s' data-x='%d' data-y='%d'></div>", css, this.x, this.y);
        }
        
    }


    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMina() {
        return mina;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getBombas() {
        return bombas;
    }

    public void setBombas(int bombas) {
        this.bombas = bombas;
    }

    


    

}

